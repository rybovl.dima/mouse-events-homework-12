// 1. Додати новий абзац по кліку на кнопку:
// По кліку на кнопку <button id="btn-click">Click Me</button>, створіть новий елемент <p> з текстом "New Paragraph" і додайте його до розділу <section id="content">

const btnClick = document.getElementById('btn-click');
const newSection = document.createElement('section');
newSection.id = 'content';
document.body.append(newSection);

btnClick.addEventListener('click', function (){
    let p = document.createElement('p');
    p.textContent = "New Paragraph";
    newSection.append(p);
});

// 2. Додати новий елемент форми із атрибутами:
// Створіть кнопку з id "btn-input-create", додайте її на сторінку в section перед footer.
// По кліку на створену кнопку, створіть новий елемент <input> і додайте до нього власні атрибути, наприклад, type, placeholder, і name. та додайте його під кнопкою.

const newFooter = document.createElement("footer");
const newSection2 = document.createElement("section");
const newButton = document.createElement("button");
newButton.id = "btn-input-create";
newButton.textContent = "Create input";


document.body.append(newFooter);
newFooter.before(newSection2);
newSection2.append(newButton);
newSection2.style.display = "flex";
newSection2.style.flexDirection = "column";
newSection2.style.width = "200px";

newButton.addEventListener("click", function() {
    let newInput = document.createElement("input");
    newInput.setAttribute("text", "text");
    newInput.setAttribute("name", "name");
    newInput.setAttribute("placeholder", "Enter your text");
    newButton.after(newInput);
})




